package me.mitkovic.kotlin.remotewave.data.repository

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.network.DeviceDiscoveryClient
import javax.inject.Inject

class DeviceDiscoveryRepositoryImpl @Inject constructor(
    private val deviceDiscoveryClient: DeviceDiscoveryClient,
) : DeviceDiscoveryRepository {

    override fun search(): Flow<Device> = deviceDiscoveryClient.search()
}
