package me.mitkovic.kotlin.remotewave.ui.commons

import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect

@Composable
fun HandleErrorSnackbar(
    snackbarHostState: SnackbarHostState,
    errorMessage: Pair<String, Long>?,
) {
    // Use the timestamp as the key for LaunchedEffect
    LaunchedEffect(errorMessage?.second) {
        errorMessage?.first?.let { message ->
            // Dismiss any existing snackbar
            snackbarHostState.currentSnackbarData?.dismiss()
            // Show the snackbar
            snackbarHostState.showSnackbar(
                message = message,
                duration = SnackbarDuration.Short,
            )
        }
    }
}
