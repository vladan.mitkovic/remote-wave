package me.mitkovic.kotlin.remotewave.data.repository

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.remotewave.data.model.Device

interface DataStoreRepository {

    suspend fun saveDevice(device: Device)

    fun getDevice(): Flow<Device?>

    suspend fun saveCookies(cookieString: String)

    fun getCookies(): Flow<String?>
}
