package me.mitkovic.kotlin.remotewave.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import me.mitkovic.kotlin.remotewave.logging.TimberLogger
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LoggingModule {

    @Provides
    @Singleton
    fun provideAppLogger(): AppLogger = TimberLogger()
}
