package me.mitkovic.kotlin.remotewave.data.model

data class DeviceDescription(
    val friendlyName: String,
    val manufacturer: String,
    val manufacturerURL: String,
    val modelName: String,
    val udn: String,
    val icons: List<Icon>,
    val services: List<Service>,
    val scalarWebAPIServices: List<ScalarWebAPIService>,
    val irccCodes: List<IRCCCode>,
)

data class Icon(
    val mimetype: String,
    val width: Int,
    val height: Int,
    val depth: Int,
    val url: String,
)

data class IRCCCode(
    val command: String,
    val code: String,
)

data class ScalarWebAPIService(
    val serviceType: String,
)

data class Service(
    val serviceType: String,
    val serviceId: String,
    val scpdUrl: String,
    val controlUrl: String,
    val eventSubUrl: String?,
)
