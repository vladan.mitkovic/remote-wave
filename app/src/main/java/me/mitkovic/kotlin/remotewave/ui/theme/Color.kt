package me.mitkovic.kotlin.remotewave.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val SurfaceDark = Color(0xFF16171c)
val PinkBack40 = Color(0xFF1d1e25) // 0xFF2F4F4F//0xFF0F0138
val OnPrimaryDark = Color(0xFF1f232b) // 0xFF5BC6CD//0xFF1f232b
val OnSecondaryDark = Color(0xFF5BC6CD) // 0xFF5BC6CD//0xFF1f232b
