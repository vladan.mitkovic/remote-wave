package me.mitkovic.kotlin.remotewave.testing.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository

object TestDataStoreRepositoryImpl : DataStoreRepository {

    private val _cookies = MutableStateFlow<String?>(null)
    private val _device = MutableStateFlow<Device?>(null)

    fun setDevice(device: Device) {
        _device.value = device
    }

    override suspend fun saveDevice(device: Device) {
        _device.value = device
    }

    override fun getDevice(): Flow<Device?> = _device

    override suspend fun saveCookies(cookieString: String) {
        _cookies.value = cookieString
    }

    override fun getCookies(): Flow<String?> = _cookies
}
