package me.mitkovic.kotlin.remotewave.testing.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DeviceDiscoveryRepository

object TestDeviceDiscoveryRepositoryImpl : DeviceDiscoveryRepository {

    private val _device = MutableStateFlow<Device?>(null)
    val device = _device.asStateFlow()

    fun setDevice(device: Device) {
        _device.value = device
    }

    override fun search(): Flow<Device?> = device
}
