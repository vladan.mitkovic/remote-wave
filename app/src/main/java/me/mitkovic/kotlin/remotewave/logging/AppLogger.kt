package me.mitkovic.kotlin.remotewave.logging

interface AppLogger {

    fun logDebug(
        tag: String,
        message: String,
    )

    fun logInfo(
        tag: String,
        message: String,
    )

    fun logError(
        tag: String,
        message: String?,
        throwable: Throwable?,
    )
}
