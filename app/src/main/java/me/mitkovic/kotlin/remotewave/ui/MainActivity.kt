package me.mitkovic.kotlin.remotewave.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.remotewave.R
import me.mitkovic.kotlin.remotewave.common.ConnectivityObserver
import me.mitkovic.kotlin.remotewave.ui.commons.HandleErrorSnackbar
import me.mitkovic.kotlin.remotewave.ui.navigation.Destination
import me.mitkovic.kotlin.remotewave.ui.navigation.RemoteWaveAppNavigation
import me.mitkovic.kotlin.remotewave.ui.theme.RemoteWaveTheme
import javax.inject.Inject

sealed class MainAction {
    data class TitleTextChanged(
        val title: String,
    ) : MainAction()

    data class ShowActionsChanged(
        val showActions: Boolean,
    ) : MainAction()

    data class ShowBackIconChanged(
        val showBackButton: Boolean,
    ) : MainAction()

    data class OnError(
        val error: String,
    ) : MainAction()
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var connectivityObserver: ConnectivityObserver

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        setContent {
            RemoteWaveTheme {
                MainScreen(connectivityObserver)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(connectivityObserver: ConnectivityObserver) {
    val context = LocalContext.current
    val networkStatus by connectivityObserver.observe().collectAsState(initial = null)
    val navController = rememberNavController()
    val topBarTitle = remember { mutableStateOf(context.getString(R.string.app_name)) }
    val showActions = remember { mutableStateOf(false) }
    val showBackIcon = remember { mutableStateOf(false) }
    val snackbarHostState = remember { SnackbarHostState() }
    val errorMessageState = remember { mutableStateOf<Pair<String, Long>?>(null) }
    val scope = rememberCoroutineScope()

    Scaffold(
        modifier =
            Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.surface),
        topBar = {
            TopAppBar(
                title = { ApplicationTitle(topBarTitle.value, showActions.value) },
                actions = {
                    if (showActions.value) {
                        IconButton(onClick = { navController.navigate(Destination.DiscoverDevices) }) {
                            Icon(
                                Icons.Default.Settings,
                                modifier = Modifier.size(dimensionResource(id = R.dimen.icon_size)),
                                contentDescription = stringResource(R.string.content_description_discovery_icon),
                                tint = MaterialTheme.colorScheme.onSecondary,
                            )
                        }
                    }
                },
                navigationIcon = {
                    if (showBackIcon.value) {
                        IconButton(onClick = { navController.popBackStack() }) {
                            Icon(
                                Icons.AutoMirrored.Filled.ArrowBack,
                                modifier = Modifier.size(dimensionResource(id = R.dimen.icon_size)),
                                contentDescription = stringResource(R.string.content_description_back_arrow),
                                tint = MaterialTheme.colorScheme.onSecondary,
                            )
                        }
                    }
                },
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.background),
            )
        },
        bottomBar = { NetworkStatusIndicator(networkStatus, snackbarHostState, scope) },
        snackbarHost = { SnackbarHost(snackbarHostState) },
    ) { paddingValues ->
        RemoteWaveAppNavigation(navController, paddingValues, onAction = { action ->
            when (action) {
                is MainAction.TitleTextChanged -> topBarTitle.value = action.title
                is MainAction.ShowActionsChanged -> showActions.value = action.showActions
                is MainAction.ShowBackIconChanged -> showBackIcon.value = action.showBackButton
                is MainAction.OnError -> errorMessageState.value = action.error to System.currentTimeMillis()
            }
        })
    }
    errorMessageState.value?.let { errorMessage ->
        HandleErrorSnackbar(snackbarHostState = snackbarHostState, errorMessage = errorMessage)
    }
}

@Composable
fun ApplicationTitle(
    title: String,
    showActions: Boolean,
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxWidth(),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth(),
        ) {
            if (showActions) {
                Icon(
                    painter = painterResource(id = R.drawable.remote_wave_app_icon),
                    contentDescription = stringResource(R.string.content_description_application_icon),
                    modifier = Modifier.size(dimensionResource(id = R.dimen.icon_size)),
                    tint = MaterialTheme.colorScheme.onSecondary,
                )
                Spacer(modifier = Modifier.width(dimensionResource(id = R.dimen.icon_size)))
            }
            Box(
                contentAlignment = Alignment.Center,
                modifier =
                    Modifier
                        .weight(1f),
            ) {
                Text(
                    text = title,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.titleLarge.copy(color = MaterialTheme.colorScheme.onSecondary),
                )
            }
        }
    }
}

@Composable
fun NetworkStatusIndicator(
    status: ConnectivityObserver.Status?,
    snackbarHostState: SnackbarHostState,
    scope: CoroutineScope,
) {
    Column {
        // Box with network status
        Box(
            modifier =
                Modifier
                    .fillMaxWidth()
                    .height(dimensionResource(id = R.dimen.icon_size_large))
                    .background(MaterialTheme.colorScheme.surface)
                    .padding(end = dimensionResource(id = R.dimen.spacing_medium)),
            contentAlignment = Alignment.CenterEnd,
        ) {
            val (message, networkIconColor, icon) =
                when (status?.ordinal) {
                    0 -> Triple("Connected to internet", Color.Green, Icons.Default.CheckCircle)
                    1 -> Triple("No internet connection", Color.Red, Icons.Default.Info)
                    else -> Triple("Network status unknown", Color.Gray, Icons.Default.Info)
                }
            IconButton(
                onClick = {
                    scope.launch {
                        snackbarHostState.showSnackbar(
                            message = message,
                            duration = SnackbarDuration.Short,
                        )
                    }
                },
                modifier = Modifier.size(dimensionResource(id = R.dimen.spacing_medium)),
            ) {
                Icon(
                    icon,
                    contentDescription = stringResource(R.string.content_description_network_status),
                    tint = networkIconColor,
                )
            }
        }
    }
}
