package me.mitkovic.kotlin.remotewave.ui.screens.discovery

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import me.mitkovic.kotlin.remotewave.R
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.ui.MainAction
import me.mitkovic.kotlin.remotewave.ui.theme.RemoteWaveTheme

@Composable
fun DiscoveryScreen(
    viewModel: DiscoveryViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    onAction: (MainAction) -> Unit,
    onBack: () -> Unit,
) {
    val uiDevicesState by viewModel.uiDevicesState.collectAsState()

    uiDevicesState.errorMessage?.let { error ->
        onAction(MainAction.OnError(error))
        viewModel.clearError()
    }

    DiscoveryScreenContent(
        discoveredDevices = uiDevicesState.devices.devices,
        isLoading = uiDevicesState.isLoading,
        paddingValues = paddingValues,
        onStartDiscovery = { viewModel.startDiscovery() },
        onDeviceSelected = { device ->
            viewModel.selectDevice(device)
            onBack()
        },
    )
}

@Composable
fun DiscoveryScreenContent(
    discoveredDevices: List<Device>,
    isLoading: Boolean,
    paddingValues: PaddingValues,
    onStartDiscovery: () -> Unit,
    onDeviceSelected: (Device) -> Unit,
) {
    Column(
        modifier =
            Modifier
                .padding(paddingValues)
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.surface)
                .padding(dimensionResource(id = R.dimen.spacing_medium)),
    ) {
        Text(
            text = stringResource(R.string.discovery_instruction),
            color = Color.White,
        )

        Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_medium)))

        Box(
            contentAlignment = Alignment.Center,
            modifier =
                Modifier
                    .background(MaterialTheme.colorScheme.onPrimary, RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                    .clickable {
                        onStartDiscovery()
                    }.padding(dimensionResource(id = R.dimen.spacing_medium))
                    .fillMaxWidth(),
        ) {
            Text(
                text = stringResource(R.string.discover_tvs),
                color = Color.White,
            )
        }

        Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_medium)))

        if (isLoading) {
            // Display a loader
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center,
            ) {
                CircularProgressIndicator()
            }
        } else {
            if (discoveredDevices.isEmpty()) {
                // Display a message when no devices are found
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center,
                ) {
                    Text(text = stringResource(R.string.no_devices_found), color = Color.White)
                }
            } else {
                LazyColumn {
                    items(discoveredDevices) { device ->
                        Text(
                            text = device.friendlyName ?: stringResource(R.string.default_friendly_name),
                            color = Color.White,
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .clickable {
                                        onDeviceSelected(device)
                                    }.padding(dimensionResource(id = R.dimen.spacing_small)),
                        )
                        HorizontalDivider()
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DiscoveryScreenContentPreview() {
    RemoteWaveTheme {
        DiscoveryScreenContent(
            discoveredDevices =
                listOf(
                    Constants.testDevice,
                    Constants.testDevice,
                    Constants.testDevice,
                ),
            isLoading = false,
            paddingValues = PaddingValues(),
            onStartDiscovery = {},
            onDeviceSelected = {},
        )
    }
}
