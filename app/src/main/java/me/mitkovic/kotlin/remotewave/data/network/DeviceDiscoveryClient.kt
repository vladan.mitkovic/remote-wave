package me.mitkovic.kotlin.remotewave.data.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DeviceRepository
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import java.io.IOException
import java.io.InterruptedIOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.net.SocketTimeoutException
import javax.inject.Inject

class DeviceDiscoveryClient @Inject constructor(
    private val deviceRepository: DeviceRepository,
    private val logger: AppLogger,
) {

    @Volatile
    private var mSearching = false

    fun search(): Flow<Device> =
        flow {
            if (mSearching) {
                logger.logDebug(TAG, Constants.SEARCH_ALREADY_SEARCHING)
                return@flow
            }
            logger.logInfo(TAG, Constants.SEARCH_START)

            // SSDP (Simple Service Discovery Protocol)
            val ssdpRequest = (
                "M-SEARCH * HTTP/1.1\r\n" +
                    "HOST: $SSDP_ADDR:$SSDP_PORT\r\n" +
                    "MAN: \"ssdp:discover\"\r\n" +
                    "MX: $SSDP_MX\r\n" +
                    "ST: $SSDP_ST\r\n\r\n"
            )

            val sendData = ssdpRequest.toByteArray()

            val socket = DatagramSocket()
            val iAddress = InetSocketAddress(SSDP_ADDR, SSDP_PORT)
            val packet = DatagramPacket(sendData, sendData.size, iAddress)
            socket.send(packet)
            Thread.sleep(100)
            socket.send(packet)
            Thread.sleep(100)
            socket.send(packet)

            mSearching = true
            val startTime = System.currentTimeMillis()
            val foundDevices = ArrayList<String?>()
            val array = ByteArray(PACKET_BUFFER_SIZE)
            while (mSearching) {
                val receivePacket = DatagramPacket(array, array.size)
                try {
                    socket.soTimeout = SSDP_RECEIVE_TIMEOUT
                    socket.receive(receivePacket)
                    val ssdpReplyMessage = String(receivePacket.data, 0, receivePacket.length)
                    val ddUsn = findParameterValue(ssdpReplyMessage, "USN")

                    if (!foundDevices.contains(ddUsn)) {
                        val ddLocation = findParameterValue(ssdpReplyMessage, "LOCATION")
                        foundDevices.add(ddUsn)
                        logger.logDebug(TAG, Constants.DD_LOCATION_LOG.format(ddLocation))

                        val device = deviceRepository.fetch(ddLocation)
                        if (device != null) {
                            emit(device) // Emit the device immediately when found
                        }
                    }
                } catch (e: SocketTimeoutException) {
                    logger.logError(TAG, Constants.SEARCH_TIMEOUT.format(e.message), e)
                    break
                } catch (e: InterruptedIOException) {
                    logger.logError(TAG, Constants.SEARCH_INTERRUPTED.format(e.message), e)
                    break
                } catch (e: IOException) {
                    logger.logError(TAG, Constants.SEARCH_IO_EXCEPTION.format(e.message), e)
                    mSearching = false
                    socket.close()
                    throw e
                }

                if (SSDP_RECEIVE_TIMEOUT < System.currentTimeMillis() - startTime) {
                    break
                }
            }
            mSearching = false
            if (!socket.isClosed) {
                socket.close()
            }
        }.flowOn(Dispatchers.IO)

    companion object {
        private val TAG = DeviceDiscoveryClient::class.java.simpleName

        private const val SSDP_RECEIVE_TIMEOUT = 10000 // msec
        private const val PACKET_BUFFER_SIZE = 1024
        private const val SSDP_PORT = 1900
        private const val SSDP_MX = 1
        private const val SSDP_ADDR = "239.255.255.250"
        private const val SSDP_ST = "urn:schemas-sony-com:service:ScalarWebAPI:1"

        internal fun findParameterValue(
            ssdpMessage: String,
            paramName: String,
        ): String? {
            var name = paramName
            if (!name.endsWith(":")) {
                name += ":"
            }
            val start = ssdpMessage.indexOf(name)
            val end = ssdpMessage.indexOf("\r\n", start)
            return if (start != -1 && end != -1) {
                val startIdx = start + name.length
                val value = ssdpMessage.substring(startIdx, end)
                value.trim { it <= ' ' }
            } else {
                null
            }
        }
    }
}
