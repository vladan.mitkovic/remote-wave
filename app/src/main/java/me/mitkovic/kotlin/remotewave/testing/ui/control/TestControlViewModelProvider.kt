package me.mitkovic.kotlin.remotewave.testing.ui.control

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDataStoreRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.logging.TestAppLoggerImpl
import me.mitkovic.kotlin.remotewave.testing.tvcontrol.TestTVControlServiceImpl
import me.mitkovic.kotlin.remotewave.ui.screens.control.ControlViewModel

object TestControlViewModelProvider {

    fun provideFakeControlViewModel(ioDispatcher: CoroutineDispatcher = Dispatchers.IO): ControlViewModel =
        ControlViewModel(
            tvControlService = TestTVControlServiceImpl,
            dataStoreRepository = TestDataStoreRepositoryImpl,
            logger = TestAppLoggerImpl,
            ioDispatcher = ioDispatcher,
        )
}
