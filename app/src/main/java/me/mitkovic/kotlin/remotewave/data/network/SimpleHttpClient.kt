package me.mitkovic.kotlin.remotewave.data.network

import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.SocketTimeoutException
import java.net.URL
import javax.inject.Inject

class SimpleHttpClient @Inject constructor(
    private val logger: AppLogger,
) {

    private val TAG = SimpleHttpClient::class.java.simpleName

    companion object {
        private const val DEFAULT_CONNECTION_TIMEOUT = 10000
        private const val DEFAULT_READ_TIMEOUT = 10000
    }

    @Throws(IOException::class)
    fun httpGet(url: String): String = httpGet(url, DEFAULT_READ_TIMEOUT)

    @Throws(IOException::class)
    fun httpGet(
        url: String,
        timeout: Int,
    ): String {
        var httpConn: HttpURLConnection? = null
        var inputStream: InputStream? = null

        // Open connection and input stream
        try {
            val _url = URL(url)
            httpConn = _url.openConnection() as HttpURLConnection
            httpConn.requestMethod = "GET"
            httpConn.connectTimeout = DEFAULT_CONNECTION_TIMEOUT
            httpConn.readTimeout = timeout
            httpConn.connect()

            val responseCode = httpConn.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                inputStream = httpConn.inputStream
            }
            if (inputStream == null) {
                val exception = IOException(Constants.HTTP_GET_RESPONSE_ERROR.format(responseCode))
                logger.logError(TAG, Constants.HTTP_GET_RESPONSE_CODE_ERROR.format(responseCode, url), exception)
                throw exception
            }
        } catch (e: SocketTimeoutException) {
            logger.logError(TAG, Constants.HTTP_GET_TIMEOUT.format(url), e)
            throw IOException()
        } catch (e: MalformedURLException) {
            logger.logError(TAG, Constants.HTTP_GET_MALFORMED_URL.format(url), e)
            throw IOException()
        } catch (e: IOException) {
            logger.logError(TAG, Constants.HTTP_GET_IO_EXCEPTION.format(e.message), e)
            httpConn?.disconnect()
            throw e
        }

        // Read stream as String
        var reader: BufferedReader? = null
        try {
            val responseBuf = StringBuilder()
            reader = BufferedReader(InputStreamReader(inputStream))
            var c: Int
            while (reader.read().also { c = it } != -1) {
                responseBuf.append(c.toChar())
            }
            return responseBuf.toString()
        } catch (e: IOException) {
            logger.logError(TAG, Constants.HTTP_GET_IO_EXCEPTION.format(e.message), e)
            throw e
        } finally {
            try {
                reader?.close()
            } catch (e: IOException) {
                logger.logError(TAG, Constants.CLOSE_BUFFERED_READER_ERROR, e)
            }
            try {
                inputStream.close()
            } catch (e: IOException) {
                logger.logError(TAG, Constants.CLOSE_INPUT_STREAM_ERROR, e)
            }
        }
    }

    @Throws(IOException::class)
    fun httpPost(
        url: String,
        postData: String,
    ): String = httpPost(url, postData, DEFAULT_READ_TIMEOUT)

    @Throws(IOException::class)
    fun httpPost(
        url: String,
        postData: String,
        timeout: Int,
    ): String {
        var httpConn: HttpURLConnection? = null
        var outputStream: OutputStream? = null
        var writer: OutputStreamWriter? = null
        var inputStream: InputStream? = null

        // Open connection and input stream
        try {
            val _url = URL(url)
            httpConn = _url.openConnection() as HttpURLConnection
            httpConn.requestMethod = "POST"
            httpConn.connectTimeout = DEFAULT_CONNECTION_TIMEOUT
            httpConn.readTimeout = timeout
            httpConn.doInput = true
            httpConn.doOutput = true

            outputStream = httpConn.outputStream
            writer = OutputStreamWriter(outputStream, "UTF-8")
            writer.write(postData)
            writer.flush()
            writer.close()
            writer = null
            outputStream.close()
            outputStream = null

            val responseCode = httpConn.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                inputStream = httpConn.inputStream
            }
            if (inputStream == null) {
                val exception = IOException(Constants.HTTP_GET_RESPONSE_ERROR.format(responseCode))
                logger.logError(TAG, Constants.HTTP_POST_RESPONSE_CODE_ERROR.format(responseCode, url), exception)
                throw exception
            }
        } catch (e: SocketTimeoutException) {
            logger.logError(TAG, Constants.HTTP_POST_TIMEOUT.format(url), e)
            throw IOException()
        } catch (e: MalformedURLException) {
            logger.logError(TAG, Constants.HTTP_POST_MALFORMED_URL.format(url), e)
            throw IOException()
        } catch (e: IOException) {
            logger.logError(TAG, Constants.HTTP_POST_IO_EXCEPTION.format(e.message), e)
            httpConn?.disconnect()
            throw e
        } finally {
            try {
                writer?.close()
            } catch (e: IOException) {
                logger.logError(TAG, Constants.CLOSE_OUTPUT_STREAM_WRITER_ERROR, e)
            }
            try {
                outputStream?.close()
            } catch (e: IOException) {
                logger.logError(TAG, Constants.CLOSE_OUTPUT_STREAM_ERROR, e)
            }
        }

        // Read stream as String
        var reader: BufferedReader? = null
        try {
            val responseBuf = StringBuilder()
            reader = BufferedReader(InputStreamReader(inputStream))
            var c: Int
            while (reader.read().also { c = it } != -1) {
                responseBuf.append(c.toChar())
            }
            return responseBuf.toString()
        } catch (e: IOException) {
            logger.logError(TAG, Constants.HTTP_POST_READ_ERROR.format(e.message), e)
            throw e
        } finally {
            try {
                reader?.close()
            } catch (e: IOException) {
                logger.logError(TAG, Constants.CLOSE_BUFFERED_READER_ERROR, e)
            }
        }
    }
}
