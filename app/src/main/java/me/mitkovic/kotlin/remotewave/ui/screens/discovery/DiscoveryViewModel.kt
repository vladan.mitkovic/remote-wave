package me.mitkovic.kotlin.remotewave.ui.screens.discovery

import androidx.compose.runtime.Immutable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository
import me.mitkovic.kotlin.remotewave.data.repository.DeviceDiscoveryRepository
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import javax.inject.Inject
import kotlin.coroutines.cancellation.CancellationException

data class DevicesState(
    val isLoading: Boolean = false,
    val devices: Devices = Devices(emptyList()),
    val errorMessage: String? = null,
)

@Immutable
data class Devices(
    val devices: List<Device>,
)

@HiltViewModel
class DiscoveryViewModel @Inject constructor(
    private val deviceDiscoveryRepository: DeviceDiscoveryRepository,
    private val dataStoreRepository: DataStoreRepository,
    private val logger: AppLogger,
) : ViewModel() {

    companion object {
        private const val TAG = Constants.DISCOVERY_VIEW_MODEL_TAG
    }

    val _uiDevicesState = MutableStateFlow(DevicesState())
    val uiDevicesState: StateFlow<DevicesState>
        get() = _uiDevicesState.asStateFlow()

    fun startDiscovery() {
        viewModelScope.launch {
            _uiDevicesState.value = _uiDevicesState.value.copy(isLoading = true, errorMessage = null)
            try {
                deviceDiscoveryRepository.search().collect { device ->
                    device?.let {
                        _uiDevicesState.value =
                            _uiDevicesState.value.copy(
                                devices = Devices(_uiDevicesState.value.devices.devices + it),
                                isLoading = false,
                                errorMessage = null,
                            )
                    }
                }
            } catch (e: CancellationException) {
                logger.logError(TAG, Constants.DISCOVERY_JOB_CANCELLED, e)
            } catch (e: Exception) {
                logger.logError(TAG, Constants.ERROR_DURING_DISCOVERY + e.message, e)
                _uiDevicesState.value =
                    _uiDevicesState.value.copy(
                        errorMessage = e.localizedMessage ?: Constants.UNKNOWN_ERROR,
                        isLoading = false,
                    )
            }
        }
    }

    fun selectDevice(device: Device) {
        viewModelScope.launch {
            dataStoreRepository.saveDevice(device)
        }
    }

    fun clearError() {
        _uiDevicesState.value = _uiDevicesState.value.copy(errorMessage = null)
    }
}
