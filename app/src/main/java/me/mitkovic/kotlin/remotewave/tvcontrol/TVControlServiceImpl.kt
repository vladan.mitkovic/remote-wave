package me.mitkovic.kotlin.remotewave.tvcontrol

import kotlinx.coroutines.flow.first
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.common.Constants.irccCodeMap
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.net.URL
import javax.inject.Inject

class TVControlServiceImpl @Inject constructor(
    private val httpClient: OkHttpClient,
    private val dataStoreRepository: DataStoreRepository,
    private val logger: AppLogger,
) : TVControlService {

    private var tvIpAddress: String = ""
    private var tvPort: String = "8080"
    private var controlUrl: String = ""

    companion object {
        private const val TAG = Constants.CONTROL_SERVICE_TAG
    }

    override suspend fun authenticate(device: Device): Result<Unit> =
        try {
            val accessControlUrl = device.apiServices.find { it.name == "urn:schemas-sony-com:service:ScalarWebAPI:1" }?.actionUrl ?: ""
            val url = "$accessControlUrl/accessControl"
            val requestBody =
                """
                {
                    "id": 1,
                    "version": "1.0",
                    "method": "actRegister",
                    "params": [
                        {
                            "clientid": "SonyRemoteControl:14a42229-af3d-40e7-b1b2-743331375368c",
                            "nickname": "SonyRemoteControl",
                            "level": "private"
                        },
                        [
                            {
                                "clientid": "SonyRemoteControl:14a42229-af3d-40e7-b1b2-743331375368c",
                                "value": "yes",
                                "nickname": "SonyRemoteControl",
                                "function": "WOL"
                            }
                        ]
                    ]
                }
                """.trimIndent().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

            val request =
                Request
                    .Builder()
                    .url(url)
                    .post(requestBody)
                    .build()

            logger.logDebug(TAG, Constants.SENDING_AUTH_REQUEST.format(url))

            httpClient.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    val exception = IOException("Unexpected code $response")
                    logger.logError(TAG, Constants.AUTH_FAILED.format(response.code, response.message), exception)
                    throw exception
                }
                logger.logDebug(TAG, Constants.AUTH_SUCCESSFUL)

                response.body?.let { responseBody ->
                    val responseString = responseBody.string()
                    logger.logDebug(TAG, Constants.RESPONSE_BODY.format(responseString))

                    // Capture cookies from the response
                    val cookies = response.headers("Set-Cookie").joinToString("; ")
                    logger.logDebug(TAG, Constants.COOKIES.format(cookies))

                    saveCookies(cookies)
                }
            }
            Result.success(Unit)
        } catch (e: Exception) {
            logger.logError(TAG, Constants.AUTH_ERROR.format(e.message), e)
            Result.failure(e)
        }

    override fun setTvIpAddress(device: Device) {
        val url = URL(device.ddUrl)
        tvIpAddress = url.host
        tvPort = if (url.port == -1) "8080" else url.port.toString()
        controlUrl = device.apiServices.find { it.name == "urn:schemas-sony-com:service:IRCC:1" }?.actionUrl ?: ""

        logger.logDebug(TAG, Constants.SETTING_TV_IP.format(tvIpAddress, tvPort))
        logger.logDebug(TAG, Constants.SETTING_CONTROL_URL.format(controlUrl))
    }

    override suspend fun sendCommandToTv(command: String) {
        if (tvIpAddress.isEmpty() || controlUrl.isEmpty()) {
            logger.logDebug(TAG, Constants.TV_IP_OR_CONTROL_URL_EMPTY)
            return
        }

        val irccCode =
            irccCodeMap[command] ?: run {
                logger.logDebug(TAG, Constants.IRCC_CODE_NOT_FOUND.format(command))
                return
            }

        val soapBody =
            """
            <?xml version="1.0"?>
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <s:Body>
                    <u:X_SendIRCC xmlns:u="urn:schemas-sony-com:service:IRCC:1">
                        <IRCCCode>$irccCode</IRCCCode>
                    </u:X_SendIRCC>
                </s:Body>
            </s:Envelope>
            """.trimIndent()

        val requestBody = soapBody.toRequestBody("text/xml; charset=utf-8".toMediaTypeOrNull())
        val savedCookies = getSavedCookies()
        val contentLength = requestBody.contentLength()

        val request =
            Request
                .Builder()
                .url(controlUrl)
                .post(requestBody)
                .header("Accept", "*/*")
                .header("Content-Type", "text/xml; charset=UTF-8")
                .header("SOAPACTION", "\"urn:schemas-sony-com:service:IRCC:1#X_SendIRCC\"")
                .header("X-Auth-PSK", "1234")
                .header("Connection", "Keep-Alive")
                .header("Content-Length", contentLength.toString())
                .apply {
                    savedCookies?.let { header("Cookie", it) }
                }.build()

        try {
            httpClient.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    val exception = IOException("Unexpected code $response")
                    logger.logError(TAG, Constants.SONY_API_COMMAND_FAILED.format(response.code, response.message), exception)
                    throw IOException("Unexpected code $response")
                }
                logger.logDebug(TAG, Constants.SONY_API_COMMAND_SUCCESSFUL)
            }
        } catch (e: Exception) {
            logger.logError(TAG, Constants.ERROR_SENDING_SONY_API_COMMAND, e)
        }
    }

    private suspend fun saveCookies(cookieString: String) {
        dataStoreRepository.saveCookies(cookieString)
    }

    private suspend fun getSavedCookies(): String? = dataStoreRepository.getCookies().first()
}
