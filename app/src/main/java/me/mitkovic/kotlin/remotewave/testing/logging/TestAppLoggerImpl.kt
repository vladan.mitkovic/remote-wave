package me.mitkovic.kotlin.remotewave.testing.logging

import me.mitkovic.kotlin.remotewave.logging.AppLogger

object TestAppLoggerImpl : AppLogger {

    private val loggedDebug = mutableListOf<String>()
    private val loggedInfo = mutableListOf<String>()
    private val loggedError = mutableListOf<String>()

    override fun logDebug(
        tag: String,
        message: String,
    ) {
        loggedDebug.add("$tag: $message")
    }

    override fun logInfo(
        tag: String,
        message: String,
    ) {
        loggedInfo.add("$tag: $message")
    }

    override fun logError(
        tag: String,
        message: String?,
        throwable: Throwable?,
    ) {
        loggedError.add("$tag: $message ${throwable?.message ?: ""}")
    }

    fun getLoggedDebug(): List<String> = loggedDebug

    fun getLoggedInfo(): List<String> = loggedInfo

    fun getLoggedErrors(): List<String> = loggedError
}
