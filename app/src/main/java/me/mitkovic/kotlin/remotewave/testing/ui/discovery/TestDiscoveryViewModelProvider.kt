package me.mitkovic.kotlin.remotewave.testing.ui.discovery

import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDataStoreRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDeviceDiscoveryRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.logging.TestAppLoggerImpl
import me.mitkovic.kotlin.remotewave.ui.screens.discovery.DiscoveryViewModel

object TestDiscoveryViewModelProvider {

    fun provideFakeDiscoveryViewModel(): DiscoveryViewModel =
        DiscoveryViewModel(
            deviceDiscoveryRepository = TestDeviceDiscoveryRepositoryImpl,
            dataStoreRepository = TestDataStoreRepositoryImpl,
            logger = TestAppLoggerImpl,
        )
}
