package me.mitkovic.kotlin.remotewave.ui.navigation

import kotlinx.serialization.Serializable

sealed class Destination {
    @Serializable
    object RemoteControl : Destination()

    @Serializable
    object DiscoverDevices : Destination()
}
