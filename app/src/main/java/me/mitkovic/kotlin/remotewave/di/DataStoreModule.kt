package me.mitkovic.kotlin.remotewave.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataStoreModule {

    private const val DATASTORE_PREFERENCES_NAME = "datastore_preferences"

    @Provides
    @Singleton
    fun provideDataStore(
        @ApplicationContext appContext: Context,
    ): DataStore<Preferences> =
        PreferenceDataStoreFactory.create(produceFile = {
            appContext.preferencesDataStoreFile(DATASTORE_PREFERENCES_NAME)
        })

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideDataStoreRepository(
        dataStore: DataStore<Preferences>,
        gson: Gson,
    ): DataStoreRepository = DataStoreRepositoryImpl(dataStore, gson)
}
