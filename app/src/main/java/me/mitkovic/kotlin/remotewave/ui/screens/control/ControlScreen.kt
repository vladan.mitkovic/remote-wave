package me.mitkovic.kotlin.remotewave.ui.screens.control

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import kotlinx.coroutines.Dispatchers
import me.mitkovic.kotlin.remotewave.R
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDataStoreRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.ui.control.TestControlViewModelProvider
import me.mitkovic.kotlin.remotewave.ui.MainAction
import me.mitkovic.kotlin.remotewave.ui.theme.RemoteWaveTheme

@Composable
fun ControlScreen(
    viewModel: ControlViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    onAction: (MainAction) -> Unit,
) {
    val device by viewModel.device.collectAsState(initial = null)
    val errorMessage by viewModel.errorMessage.collectAsState()

    viewModel.logger.logDebug(Constants.CONTROL_SCREEN_TAG, Constants.CONTROL_SCREEN_DEVICE_LOG + device)

    LaunchedEffect(device) {
        device?.let {
            viewModel.connectToTV(it)
        }
    }

    errorMessage?.let { error ->
        onAction(MainAction.OnError(error))
        viewModel.clearError()
    }

    ControlScreenContent(
        device = device,
        paddingValues = paddingValues,
        onSendCommand = { command -> viewModel.sendCommandToTv(command) },
    )
}

@Composable
fun ControlScreenContent(
    device: Device?,
    paddingValues: PaddingValues,
    onSendCommand: (String) -> Unit,
) {
    var showNumberPad by remember { mutableStateOf(false) }
    var selectedNumber by remember { mutableStateOf("") }

    if (showNumberPad) {
        NumberPad(
            selectedNumber = selectedNumber,
            onNumberSelected = { number ->
                selectedNumber = number
            },
            onConfirm = {
                onSendCommand("Num$selectedNumber")
                showNumberPad = false
            },
            onCancel = {
                showNumberPad = false
            },
        )
    } else {
        Column(
            modifier =
                Modifier
                    .padding(paddingValues)
                    .fillMaxSize(),
        ) {
            if (device == null) {
                Text(
                    text = stringResource(R.string.no_devices_message),
                    modifier =
                        Modifier
                            .padding(dimensionResource(id = R.dimen.spacing_medium)),
                )
            } else {
                RemoteControl(onSendCommand, onNumberPadRequested = {
                    showNumberPad = true
                })
            }
        }
    }
}

@Composable
fun RemoteControl(
    onSendCommand: (String) -> Unit,
    onNumberPadRequested: () -> Unit,
) {
    Column(
        modifier =
            Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.surface)
                .padding(
                    top = dimensionResource(id = R.dimen.spacing_small),
                    start = dimensionResource(id = R.dimen.spacing_medium),
                    end = dimensionResource(id = R.dimen.spacing_medium),
                ),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        MenuSection(onSendCommand, modifier = Modifier.weight(1f))
        ControlSection(onSendCommand, modifier = Modifier.weight(2f))
        MainCommandsSection(onSendCommand, onNumberPadRequested, modifier = Modifier.weight(2f))
    }
}

@Composable
fun MainCommandsSection(
    onSendCommand: (String) -> Unit,
    onNumberPadRequested: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier =
            modifier.padding(
                start = dimensionResource(id = R.dimen.spacing_medium),
                end = dimensionResource(id = R.dimen.spacing_medium),
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row(
            modifier = Modifier.padding(dimensionResource(id = R.dimen.spacing_small)),
            horizontalArrangement = Arrangement.SpaceEvenly,
        ) {
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                RemoteRoundedButton(modifier = Modifier.weight(1f), "+", roundedTop = true, applyBottomPadding = false, onClick = {
                    onSendCommand("VolumeUp")
                })
                RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.volume), applyBottomPadding = false)
                RemoteRoundedButton(modifier = Modifier.weight(1f), "-", roundedBottom = true, onClick = { onSendCommand("VolumeDown") })
            }
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                RemoteRoundedButton(
                    modifier =
                        Modifier.weight(
                            1f,
                        ),
                    "PowerOff",
                    painterResource(
                        id = R.drawable.power_button,
                    ),
                    painterResource(id = R.drawable.power_button),
                    roundedAll = true,
                    isPowerButton = true,
                    onClick = {
                        onSendCommand("PowerOff")
                    },
                )
                RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.numbers), roundedAll = true, onClick = {
                    onNumberPadRequested()
                })
                RemoteRoundedButton(
                    modifier =
                        Modifier.weight(
                            1f,
                        ),
                    stringResource(
                        R.string.mute,
                    ),
                    painterResource(id = R.drawable.volume_on),
                    painterResource(id = R.drawable.volume_off),
                    roundedAll = true,
                    onClick = {
                        onSendCommand("Mute")
                    },
                )
                RemoteRoundedButton(
                    modifier = Modifier.weight(1f),
                    stringResource(R.string.tv),
                    roundedAll = true,
                    onClick = { onSendCommand("Tv") },
                )
            }
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                RemoteRoundedButton(modifier = Modifier.weight(1f), "▲", roundedTop = true, applyBottomPadding = false, onClick = {
                    onSendCommand("ChannelUp")
                })
                RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.program), applyBottomPadding = false)
                RemoteRoundedButton(modifier = Modifier.weight(1f), "▼", roundedBottom = true, onClick = { onSendCommand("ChannelDown") })
            }
        }
    }
}

@Composable
fun ControlSection(
    onSendCommand: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier =
            modifier
                .fillMaxHeight()
                .aspectRatio(1f)
                .padding(dimensionResource(id = R.dimen.spacing_small))
                .background(MaterialTheme.colorScheme.onPrimary, shape = CircleShape),
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center,
        ) {
            DirectionButton(
                modifier = Modifier.align(Alignment.TopCenter),
                label = "▲",
                onClick = { onSendCommand("Up") },
            )
            DirectionButton(
                modifier = Modifier.align(Alignment.CenterStart),
                label = "◀",
                onClick = { onSendCommand("Left") },
            )
            DirectionButton(
                modifier = Modifier.align(Alignment.CenterEnd),
                label = "▶",
                onClick = { onSendCommand("Right") },
            )
            DirectionButton(
                modifier = Modifier.align(Alignment.BottomCenter),
                label = "▼",
                onClick = { onSendCommand("Down") },
            )
            Box(
                contentAlignment = Alignment.Center,
                modifier =
                    Modifier
                        .background(MaterialTheme.colorScheme.surface, shape = CircleShape)
                        .clickable { onSendCommand("Confirm") }
                        .padding(dimensionResource(id = R.dimen.central_button))
                        .align(Alignment.Center),
            ) {
                Text(
                    text = stringResource(R.string.ok),
                    color = Color.White,
                    fontSize = dimensionResource(id = R.dimen.font_size_small).value.sp,
                )
            }
        }
    }
}

@Composable
fun MenuSection(
    onSendCommand: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier =
            modifier
                .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
        ) {
            RemoteRoundedButton(
                modifier = Modifier.weight(1f),
                stringResource(R.string.home),
                roundedAll = true,
                onClick = { onSendCommand("Home") },
            )
            RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.netflix), roundedAll = true, onClick = {
                onSendCommand("Netflix")
            })
            RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.youtube), roundedAll = true, onClick = {
                onSendCommand("YouTube")
            })
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
        ) {
            RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.return_button), roundedAll = true, onClick = {
                onSendCommand("Return")
            })
            RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.apps), roundedAll = true, onClick = {
                onSendCommand("ApplicationLauncher")
            })
            RemoteRoundedButton(modifier = Modifier.weight(1f), stringResource(R.string.google_play), roundedAll = true, onClick = {
                onSendCommand("GooglePlay")
            })
        }
    }
}

@Composable
fun DirectionButton(
    modifier: Modifier = Modifier,
    label: String,
    onClick: () -> Unit = {},
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier =
            modifier
                .padding(dimensionResource(id = R.dimen.spacing_extra_small))
                .background(MaterialTheme.colorScheme.onPrimary, shape = CircleShape)
                .size(dimensionResource(id = R.dimen.icon_size))
                .clickable { onClick() },
    ) {
        Text(
            text = label,
            color = Color.White,
            fontSize = dimensionResource(id = R.dimen.font_size_medium).value.sp,
        )
    }
}

@Composable
fun RemoteRoundedButton(
    modifier: Modifier = Modifier,
    label: String = "",
    iconOn: Painter? = null,
    iconOff: Painter? = null,
    roundedTop: Boolean = false,
    roundedBottom: Boolean = false,
    roundedAll: Boolean = false,
    isPowerButton: Boolean = false,
    applyBottomPadding: Boolean = true,
    onClick: () -> Unit = {},
) {
    var isClicked by remember { mutableStateOf(false) }

    val shape =
        when {
            roundedAll -> RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium))
            roundedTop ->
                RoundedCornerShape(
                    topStart = dimensionResource(id = R.dimen.spacing_medium),
                    topEnd = dimensionResource(id = R.dimen.spacing_medium),
                )
            roundedBottom ->
                RoundedCornerShape(
                    bottomStart = dimensionResource(id = R.dimen.spacing_medium),
                    bottomEnd = dimensionResource(id = R.dimen.spacing_medium),
                )
            else -> RoundedCornerShape(0.dp)
        }

    val tint =
        if (isPowerButton && isClicked) {
            Color.Green
        } else if (isPowerButton) {
            Color.Red
        } else {
            Color.Unspecified
        }

    Box(
        contentAlignment = Alignment.Center,
        modifier =
            modifier
                .padding(
                    start = dimensionResource(id = R.dimen.spacing_extra_small),
                    end = dimensionResource(id = R.dimen.spacing_extra_small),
                    bottom = if (applyBottomPadding) dimensionResource(id = R.dimen.spacing_extra_small) else 0.dp,
                ).background(MaterialTheme.colorScheme.onPrimary, shape)
                .clickable {
                    isClicked = !isClicked
                    onClick()
                }.padding(dimensionResource(id = R.dimen.spacing_medium))
                .fillMaxWidth(),
    ) {
        if (iconOn != null && iconOff != null) {
            Image(
                painter = if (isClicked) iconOff else iconOn,
                contentDescription = null,
                colorFilter =
                    if (tint != Color.Unspecified) {
                        androidx.compose.ui.graphics.ColorFilter
                            .tint(tint)
                    } else {
                        null
                    },
            )
        } else {
            Text(text = label, color = Color.White, fontSize = dimensionResource(id = R.dimen.font_size_small).value.sp)
        }
    }
}

@Composable
fun NumberPad(
    selectedNumber: String,
    onNumberSelected: (String) -> Unit,
    onConfirm: () -> Unit,
    onCancel: () -> Unit,
) {
    var displayNumber by remember { mutableStateOf(selectedNumber) }

    Column(
        modifier =
            Modifier
                .background(MaterialTheme.colorScheme.surface)
                .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Text(text = displayNumber, fontSize = dimensionResource(id = R.dimen.font_size_medium).value.sp, color = Color.White)

        Column {
            listOf(
                listOf("1", "2", "3"),
                listOf("4", "5", "6"),
                listOf("7", "8", "9"),
                listOf("<-", "0", "OK"),
            ).forEach { row ->
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                ) {
                    row.forEach { label ->
                        RemoteRoundedButton(
                            label = label,
                            modifier = Modifier.width(dimensionResource(id = R.dimen.spacing_xxlarge)),
                            roundedAll = true,
                            onClick = {
                                when (label) {
                                    "<-" -> if (displayNumber.isNotEmpty()) displayNumber = displayNumber.dropLast(1)
                                    "OK" -> onConfirm()
                                    else -> displayNumber += label
                                }
                                onNumberSelected(displayNumber)
                            },
                        )
                    }
                }
            }
        }

        Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_small)))

        RemoteRoundedButton(
            label = "Cancel",
            modifier =
                Modifier.padding(
                    start = dimensionResource(id = R.dimen.spacing_medium),
                    end = dimensionResource(id = R.dimen.spacing_medium),
                ),
            roundedAll = true,
            onClick = onCancel,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ControlSectionPreview() {
    RemoteWaveTheme {
        ControlSection(onSendCommand = {})
    }
}

@Preview(showBackground = true)
@Composable
fun ControlScreenContentPreview() {
    RemoteWaveTheme {
        ControlScreenContent(
            device = Constants.testDevice,
            paddingValues = PaddingValues(),
            onSendCommand = {},
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TestDataStoreRepositoryImpl.setDevice(Constants.testDevice)

    RemoteWaveTheme {
        ControlScreen(
            viewModel = TestControlViewModelProvider.provideFakeControlViewModel(Dispatchers.IO),
            paddingValues = PaddingValues(),
            {},
        )
    }
}
