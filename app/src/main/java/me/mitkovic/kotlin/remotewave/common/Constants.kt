package me.mitkovic.kotlin.remotewave.common

import me.mitkovic.kotlin.remotewave.data.model.Device

object Constants {

    // tags
    const val DISCOVERY_VIEW_MODEL_TAG = "DiscoveryViewModel"
    const val CONTROL_VIEW_MODEL_TAG = "ControlViewModel"
    const val CONTROL_SCREEN_TAG = "ControlScreen"
    const val CONTROL_SERVICE_TAG = "TVControlServiceImpl"
    const val DEVICE_REPOSITORY_TAG = "DeviceRepository"

    // ViewModels constants
    const val UNKNOWN_ERROR = "Unknown error"
    const val DISCOVERY_JOB_CANCELLED = "Discovery job cancelled"
    const val ERROR_DURING_DISCOVERY = "Error during discovery: "
    const val COLLECTED_DEVICE = "Collected device: "
    const val ERROR_SENDING_COMMAND = "Error sending command to TV"
    const val CONTROL_SCREEN_DEVICE_LOG = "ControlScreen device: "

    // TVControlServiceImpl related constants
    const val SENDING_AUTH_REQUEST = "Sending authentication request to: %s"
    const val AUTH_FAILED = "Authentication failed: %d %s"
    const val AUTH_SUCCESSFUL = "Authentication successful"
    const val RESPONSE_BODY = "Response body: %s"
    const val COOKIES = "Cookies: %s"
    const val AUTH_ERROR = "Error during authentication: %s"
    const val SETTING_TV_IP = "Setting TV IP address to %s and port to %s"
    const val SETTING_CONTROL_URL = "Setting control URL to %s"
    const val TV_IP_OR_CONTROL_URL_EMPTY = "TV IP address or Control URL is empty"
    const val IRCC_CODE_NOT_FOUND = "IRCC code not found for command: %s"
    const val SONY_API_COMMAND_FAILED = "Sony API command failed: %d %s"
    const val SONY_API_COMMAND_SUCCESSFUL = "Sony API command successful"
    const val ERROR_SENDING_SONY_API_COMMAND = "Error sending Sony API command"

    // DeviceRepositoryImpl related constants
    const val DD_URL_NULL_ERROR = "ddUrl is null."
    const val FETCH_IO_EXCEPTION = "fetch: IOException."
    const val DEVICE_DESCRIPTION_XML = "Device Description XML: %s"
    const val DEVICE_LOG = "Device device: %s"
    const val FETCH_PARSING_DONE = "fetch() parsing XML done."

    // SimpleHttpClient related constants
    const val HTTP_GET_RESPONSE_ERROR = "Response Error:%d"
    const val HTTP_GET_RESPONSE_CODE_ERROR = "httpGet: Response Code Error: %d: %s"
    const val HTTP_GET_TIMEOUT = "httpGet: Timeout: %s"
    const val HTTP_GET_MALFORMED_URL = "httpGet: MalformedUrlException: %s"
    const val HTTP_GET_IO_EXCEPTION = "httpGet: IOException: %s"
    const val CLOSE_BUFFERED_READER_ERROR = "IOException while closing BufferedReader"
    const val CLOSE_INPUT_STREAM_ERROR = "IOException while closing InputStream"
    const val HTTP_POST_RESPONSE_CODE_ERROR = "httpPost: Response Code Error: %d: %s"
    const val HTTP_POST_TIMEOUT = "httpPost: Timeout: %s"
    const val HTTP_POST_MALFORMED_URL = "httpPost: MalformedUrlException: %s"
    const val HTTP_POST_IO_EXCEPTION = "httpPost: IOException: %s"
    const val CLOSE_OUTPUT_STREAM_WRITER_ERROR = "IOException while closing OutputStreamWriter"
    const val CLOSE_OUTPUT_STREAM_ERROR = "IOException while closing OutputStream"
    const val HTTP_POST_READ_ERROR = "httpPost: read error: %s"

    // DeviceDiscoveryClient related constants
    const val SEARCH_ALREADY_SEARCHING = "search() already searching."
    const val SEARCH_START = "search() Start."
    const val DD_LOCATION_LOG = "ddLocation: %s"
    const val SEARCH_TIMEOUT = "search() Timeout: %s"
    const val SEARCH_INTERRUPTED = "search() Interrupted: %s"
    const val SEARCH_IO_EXCEPTION = "search() IOException: %s"

    val testDevice =
        Device(
            ddUrl = "http://192.168.1.155:58280/sony/webapi/ssdp/dd.xml",
            friendlyName = "KD-49XG9005",
            modelName = "KD-49XG9005",
            udn = "uuid:248edc39-9ad6-48e4-b9e9-f06e107366e1",
            iconUrl = "/sony/webapi/ssdp/icon/dlna_tv_120.png",
            apiServices =
                mutableListOf(
                    Device.ApiService(name = "urn:schemas-sony-com:service:ScalarWebAPI:1", actionUrl = "http://192.168.1.155/sony"),
                    Device.ApiService(name = "urn:schemas-sony-com:service:IRCC:1", actionUrl = "http://192.168.1.155/sony/ircc"),
                ),
            irccCodes =
                mutableMapOf(
                    "Power" to "AAAAAQAAAAEAAAAVAw==",
                ),
        )

    val irccCodeMap: Map<String, String> =
        mapOf(
            "Num1" to "AAAAAQAAAAEAAAAAAw==",
            "Num2" to "AAAAAQAAAAEAAAABAw==",
            "Num3" to "AAAAAQAAAAEAAAACAw==",
            "Num4" to "AAAAAQAAAAEAAAADAw==",
            "Num5" to "AAAAAQAAAAEAAAAEAw==",
            "Num6" to "AAAAAQAAAAEAAAAFAw==",
            "Num7" to "AAAAAQAAAAEAAAAGAw==",
            "Num8" to "AAAAAQAAAAEAAAAHAw==",
            "Num9" to "AAAAAQAAAAEAAAAIAw==",
            "Num0" to "AAAAAQAAAAEAAAAJAw==",
            "Num11" to "AAAAAQAAAAEAAAAKAw==",
            "Num12" to "AAAAAQAAAAEAAAALAw==",
            "Enter" to "AAAAAQAAAAEAAAALAw==",
            "GGuide" to "AAAAAQAAAAEAAAAOAw==",
            "ChannelUp" to "AAAAAQAAAAEAAAAQAw==",
            "ChannelDown" to "AAAAAQAAAAEAAAARAw==",
            "VolumeUp" to "AAAAAQAAAAEAAAASAw==",
            "VolumeDown" to "AAAAAQAAAAEAAAATAw==",
            "Mute" to "AAAAAQAAAAEAAAAUAw==",
            "TvPower" to "AAAAAQAAAAEAAAAVAw==",
            "Audio" to "AAAAAQAAAAEAAAAXAw==",
            "MediaAudioTrack" to "AAAAAQAAAAEAAAAXAw==",
            "Tv" to "AAAAAQAAAAEAAAAkAw==",
            "Input" to "AAAAAQAAAAEAAAAlAw==",
            "TvInput" to "AAAAAQAAAAEAAAAlAw==",
            "TvAntennaCable" to "AAAAAQAAAAEAAAAqAw==",
            "WakeUp" to "AAAAAQAAAAEAAAAuAw==",
            "PowerOff" to "AAAAAQAAAAEAAAAvAw==",
            "Sleep" to "AAAAAQAAAAEAAAAvAw==",
            "Right" to "AAAAAQAAAAEAAAAzAw==",
            "Left" to "AAAAAQAAAAEAAAA0Aw==",
            "SleepTimer" to "AAAAAQAAAAEAAAA2Aw==",
            "Analog2" to "AAAAAQAAAAEAAAA4Aw==",
            "TvAnalog" to "AAAAAQAAAAEAAAA4Aw==",
            "Display" to "AAAAAQAAAAEAAAA6Aw==",
            "Jump" to "AAAAAQAAAAEAAAA7Aw==",
            "PicOff" to "AAAAAQAAAAEAAAA+Aw==",
            "PictureOff" to "AAAAAQAAAAEAAAA+Aw==",
            "Teletext" to "AAAAAQAAAAEAAAA/ Aw==",
            "Video1" to "AAAAAQAAAAEAAABAAw==",
            "Video2" to "AAAAAQAAAAEAAABBAw==",
            "AnalogRgb1" to "AAAAAQAAAAEAAABDAw==",
            "Home" to "AAAAAQAAAAEAAABgAw==",
            "Exit" to "AAAAAQAAAAEAAABjAw==",
            "PictureMode" to "AAAAAQAAAAEAAABkAw==",
            "Confirm" to "AAAAAQAAAAEAAABlAw==",
            "Up" to "AAAAAQAAAAEAAAB0Aw==",
            "Down" to "AAAAAQAAAAEAAAB1Aw==",
            "ClosedCaption" to "AAAAAgAAAKQAAAAQAw==",
            "Component1" to "AAAAAgAAAKQAAAA2Aw==",
            "Component2" to "AAAAAgAAAKQAAAA3Aw==",
            "Wide" to "AAAAAgAAAKQAAAA9Aw==",
            "EPG" to "AAAAAgAAAKQAAABbAw==",
            "PAP" to "AAAAAgAAAKQAAAB3Aw==",
            "TenKey" to "AAAAAgAAAJcAAAAMAw==",
            "BSCS" to "AAAAAgAAAJcAAAAQAw==",
            "Ddata" to "AAAAAgAAAJcAAAAVAw==",
            "Stop" to "AAAAAgAAAJcAAAAYAw==",
            "Pause" to "AAAAAgAAAJcAAAAZAw==",
            "Play" to "AAAAAgAAAJcAAAAaAw==",
            "Rewind" to "AAAAAgAAAJcAAAAbAw==",
            "Forward" to "AAAAAgAAAJcAAAAcAw==",
            "DOT" to "AAAAAgAAAJcAAAAdAw==",
            "Rec" to "AAAAAgAAAJcAAAAgAw==",
            "Return" to "AAAAAgAAAJcAAAAjAw==",
            "Blue" to "AAAAAgAAAJcAAAAkAw==",
            "Red" to "AAAAAgAAAJcAAAAlAw==",
            "Green" to "AAAAAgAAAJcAAAAmAw==",
            "Yellow" to "AAAAAgAAAJcAAAAnAw==",
            "SubTitle" to "AAAAAgAAAJcAAAAoAw==",
            "CS" to "AAAAAgAAAJcAAAArAw==",
            "BS" to "AAAAAgAAAJcAAAAsAw==",
            "Digital" to "AAAAAgAAAJcAAAAyAw==",
            "Options" to "AAAAAgAAAJcAAAA2Aw==",
            "Media" to "AAAAAgAAAJcAAAA4Aw==",
            "Prev" to "AAAAAgAAAJcAAAA8Aw==",
            "Next" to "AAAAAgAAAJcAAAA9Aw==",
            "DpadCenter" to "AAAAAgAAAJcAAABKAw==",
            "CursorUp" to "AAAAAgAAAJcAAABPAw==",
            "CursorDown" to "AAAAAgAAAJcAAABQAw==",
            "CursorLeft" to "AAAAAgAAAJcAAABNAw==",
            "CursorRight" to "AAAAAgAAAJcAAABOAw==",
            "ShopRemoteControlForcedDynamic" to "AAAAAgAAAJcAAABqAw==",
            "FlashPlus" to "AAAAAgAAAJcAAAB4Aw==",
            "FlashMinus" to "AAAAAgAAAJcAAAB5Aw==",
            "DemoMode" to "AAAAAgAAAJcAAAB8Aw==",
            "Analog" to "AAAAAgAAAHcAAAANAw==",
            "Mode3D" to "AAAAAgAAAHcAAABNAw==",
            "DigitalToggle" to "AAAAAgAAAHcAAABSAw==",
            "DemoSurround" to "AAAAAgAAAHcAAAB7Aw==",
            "*AD" to "AAAAAgAAABoAAAA7Aw==",
            "AudioMixUp" to "AAAAAgAAABoAAAA8Aw==",
            "AudioMixDown" to "AAAAAgAAABoAAAA9Aw==",
            "PhotoFrame" to "AAAAAgAAABoAAABVAw==",
            "Tv_Radio" to "AAAAAgAAABoAAABXAw==",
            "SyncMenu" to "AAAAAgAAABoAAABYAw==",
            "Hdmi1" to "AAAAAgAAABoAAABaAw==",
            "Hdmi2" to "AAAAAgAAABoAAABbAw==",
            "Hdmi3" to "AAAAAgAAABoAAABcAw==",
            "Hdmi4" to "AAAAAgAAABoAAABdAw==",
            "TopMenu" to "AAAAAgAAABoAAABgAw==",
            "PopUpMenu" to "AAAAAgAAABoAAABhAw==",
            "OneTouchTimeRec" to "AAAAAgAAABoAAABkAw==",
            "OneTouchView" to "AAAAAgAAABoAAABlAw==",
            "DUX" to "AAAAAgAAABoAAABzAw==",
            "FootballMode" to "AAAAAgAAABoAAAB2Aw==",
            "iManual" to "AAAAAgAAABoAAAB7Aw==",
            "Netflix" to "AAAAAgAAABoAAAB8Aw==",
            "Assists" to "AAAAAgAAAMQAAAA7Aw==",
            "FeaturedApp" to "AAAAAgAAAMQAAABEAw==",
            "FeaturedAppVOD" to "AAAAAgAAAMQAAABFAw==",
            "GooglePlay" to "AAAAAgAAAMQAAABGAw==",
            "ActionMenu" to "AAAAAgAAAMQAAABLAw==",
            "Help" to "AAAAAgAAAMQAAABNAw==",
            "TvSatellite" to "AAAAAgAAAMQAAABOAw==",
            "WirelessSubwoofer" to "AAAAAgAAAMQAAAB+Aw==",
            "AndroidMenu" to "AAAAAgAAAMQAAABPAw==",
            "RecorderMenu" to "AAAAAgAAAMQAAABIAw==",
            "STBMenu" to "AAAAAgAAAMQAAABJAw==",
            "MuteOn" to "AAAAAgAAAMQAAAAsAw==",
            "MuteOff" to "AAAAAgAAAMQAAAAtAw==",
            "AudioOutput_AudioSystem" to "AAAAAgAAAMQAAAAiAw==",
            "AudioOutput_TVSpeaker" to "AAAAAgAAAMQAAAAjAw==",
            "AudioOutput_Toggle" to "AAAAAgAAAMQAAAAkAw==",
            "ApplicationLauncher" to "AAAAAgAAAMQAAAAqAw==",
            "YouTube" to "AAAAAgAAAMQAAABHAw==",
            "PartnerApp1" to "AAAAAgAACB8AAAAAAw==",
            "PartnerApp2" to "AAAAAgAACB8AAAABAw==",
            "PartnerApp3" to "AAAAAgAACB8AAAACAw==",
            "PartnerApp4" to "AAAAAgAACB8AAAADAw==",
            "PartnerApp5" to "AAAAAgAACB8AAAAEAw==",
            "PartnerApp6" to "AAAAAgAACB8AAAAFAw==",
            "PartnerApp7" to "AAAAAgAACB8AAAAGAw==",
            "PartnerApp8" to "AAAAAgAACB8AAAAHAw==",
            "PartnerApp9" to "AAAAAgAACB8AAAAIAw==",
            "PartnerApp10" to "AAAAAgAACB8AAAAJAw==",
            "PartnerApp11" to "AAAAAgAACB8AAAAKAw==",
            "PartnerApp12" to "AAAAAgAACB8AAAALAw==",
            "PartnerApp13" to "AAAAAgAACB8AAAAMAw==",
            "PartnerApp14" to "AAAAAgAACB8AAAANAw==",
            "PartnerApp15" to "AAAAAgAACB8AAAANAw==",
            "PartnerApp16" to "AAAAAgAACB8AAAAPAw==",
            "PartnerApp17" to "AAAAAgAACB8AAAAQAw==",
            "PartnerApp18" to "AAAAAgAACB8AAAARAw==",
            "PartnerApp19" to "AAAAAgAACB8AAAASAw==",
            "PartnerApp20" to "AAAAAgAACB8AAAATAw==",
        )
}
