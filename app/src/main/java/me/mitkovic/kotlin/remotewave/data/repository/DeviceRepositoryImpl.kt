package me.mitkovic.kotlin.remotewave.data.repository

import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.network.SimpleHttpClient
import me.mitkovic.kotlin.remotewave.data.utils.DeviceDescriptionParser
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import java.io.IOException
import javax.inject.Inject

class DeviceRepositoryImpl @Inject constructor(
    private val simpleHttpClient: SimpleHttpClient,
    private val deviceDescriptionParser: DeviceDescriptionParser,
    private val logger: AppLogger,
) : DeviceRepository {

    override fun fetch(ddUrl: String?): Device? {
        requireNotNull(ddUrl) { Constants.DD_URL_NULL_ERROR }

        val ddXml: String =
            try {
                simpleHttpClient.httpGet(ddUrl)
            } catch (e: IOException) {
                logger.logError(Constants.DEVICE_REPOSITORY_TAG, Constants.FETCH_IO_EXCEPTION, e)
                return null
            }

        logger.logDebug(Constants.DEVICE_REPOSITORY_TAG, Constants.DEVICE_DESCRIPTION_XML.format(ddXml))

        val deviceDescription = deviceDescriptionParser.parse(ddXml)

        return Device(
            ddUrl = ddUrl,
            friendlyName = deviceDescription.friendlyName,
            modelName = deviceDescription.modelName,
            udn = deviceDescription.udn,
            iconUrl = deviceDescription.icons.firstOrNull { it.mimetype == "image/png" }?.url,
            apiServices = deviceDescription.services.map { Device.ApiService(it.serviceType, it.controlUrl) }.toMutableList(),
            irccCodes = deviceDescription.irccCodes.associate { it.command to it.code }.toMutableMap(),
        ).also { device ->
            logger.logDebug(Constants.DEVICE_REPOSITORY_TAG, Constants.DEVICE_LOG.format(device))
            logger.logDebug(Constants.DEVICE_REPOSITORY_TAG, Constants.FETCH_PARSING_DONE)
        }
    }
}
