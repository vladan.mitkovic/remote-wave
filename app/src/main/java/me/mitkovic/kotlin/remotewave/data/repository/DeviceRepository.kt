package me.mitkovic.kotlin.remotewave.data.repository

import me.mitkovic.kotlin.remotewave.data.model.Device

interface DeviceRepository {

    fun fetch(ddUrl: String?): Device?
}
