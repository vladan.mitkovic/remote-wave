package me.mitkovic.kotlin.remotewave.ui.screens.control

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository
import me.mitkovic.kotlin.remotewave.di.IoDispatcher
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import me.mitkovic.kotlin.remotewave.tvcontrol.TVControlService
import javax.inject.Inject

@HiltViewModel
open class ControlViewModel @Inject constructor(
    private val tvControlService: TVControlService,
    dataStoreRepository: DataStoreRepository,
    val logger: AppLogger,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
) : ViewModel() {

    val _errorMessage = MutableStateFlow<String?>(null)
    val errorMessage: StateFlow<String?> = _errorMessage

    val device: StateFlow<Device?> =
        dataStoreRepository
            .getDevice()
            .onEach { device ->
                logger.logDebug(TAG, Constants.COLLECTED_DEVICE + device)
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.Eagerly,
                initialValue = null,
            )

    fun connectToTV(device: Device) {
        viewModelScope.launch(ioDispatcher) {
            val result = tvControlService.authenticate(device)
            if (result.isFailure) {
                _errorMessage.value = result.exceptionOrNull()?.localizedMessage ?: Constants.UNKNOWN_ERROR
            } else {
                tvControlService.setTvIpAddress(device)
            }
        }
    }

    fun sendCommandToTv(command: String) {
        viewModelScope.launch(ioDispatcher) {
            try {
                tvControlService.sendCommandToTv(command)
            } catch (e: Exception) {
                logger.logError(TAG, Constants.ERROR_SENDING_COMMAND, e)
                _errorMessage.value = e.localizedMessage ?: Constants.UNKNOWN_ERROR
            }
        }
    }

    fun clearError() {
        _errorMessage.value = null
    }

    companion object {
        private const val TAG = Constants.CONTROL_VIEW_MODEL_TAG
    }
}
