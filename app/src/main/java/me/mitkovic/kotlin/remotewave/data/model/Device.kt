package me.mitkovic.kotlin.remotewave.data.model

data class Device(
    var ddUrl: String? = null,
    var friendlyName: String? = null,
    var modelName: String? = null,
    var udn: String? = null,
    var iconUrl: String? = null,
    val apiServices: MutableList<ApiService> = mutableListOf(),
    val irccCodes: MutableMap<String, String> = mutableMapOf(),
) {
    data class ApiService(
        val name: String,
        var actionUrl: String,
    )
}
