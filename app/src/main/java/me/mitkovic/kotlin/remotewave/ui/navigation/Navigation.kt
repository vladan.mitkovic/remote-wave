package me.mitkovic.kotlin.remotewave.ui.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import me.mitkovic.kotlin.remotewave.R
import me.mitkovic.kotlin.remotewave.ui.MainAction
import me.mitkovic.kotlin.remotewave.ui.screens.control.ControlScreen
import me.mitkovic.kotlin.remotewave.ui.screens.control.ControlViewModel
import me.mitkovic.kotlin.remotewave.ui.screens.discovery.DiscoveryScreen
import me.mitkovic.kotlin.remotewave.ui.screens.discovery.DiscoveryViewModel

@Composable
fun RemoteWaveAppNavigation(
    navHostController: NavHostController,
    paddingValues: PaddingValues,
    onAction: (MainAction) -> Unit,
) {
    NavHost(
        navController = navHostController,
        startDestination = Destination.RemoteControl,
    ) {
        composable<Destination.RemoteControl> {
            onAction(MainAction.TitleTextChanged(stringResource(R.string.app_name)))
            onAction(MainAction.ShowActionsChanged(true))
            onAction(MainAction.ShowBackIconChanged(false))

            val controlViewModel: ControlViewModel = hiltViewModel()
            ControlScreen(
                viewModel = controlViewModel,
                paddingValues = paddingValues,
                onAction = onAction,
            )
        }

        // CurrencyConverter screen
        composable<Destination.DiscoverDevices> {
            onAction(MainAction.TitleTextChanged(stringResource(R.string.discover_devices)))
            onAction(MainAction.ShowActionsChanged(false))
            onAction(MainAction.ShowBackIconChanged(true))

            val discoveryViewModel: DiscoveryViewModel = hiltViewModel()
            DiscoveryScreen(
                viewModel = discoveryViewModel,
                paddingValues = paddingValues,
                onAction = onAction,
                onBack = {
                    navHostController.popBackStack()
                },
            )
        }
    }
}
