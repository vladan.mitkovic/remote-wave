package me.mitkovic.kotlin.remotewave.data.repository

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.remotewave.data.model.Device

interface DeviceDiscoveryRepository {

    fun search(): Flow<Device?>
}
