package me.mitkovic.kotlin.remotewave.tvcontrol

import me.mitkovic.kotlin.remotewave.data.model.Device

interface TVControlService {

    suspend fun authenticate(device: Device): Result<Unit>

    fun setTvIpAddress(device: Device)

    suspend fun sendCommandToTv(command: String)
}
