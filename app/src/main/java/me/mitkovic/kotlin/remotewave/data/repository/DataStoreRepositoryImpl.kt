package me.mitkovic.kotlin.remotewave.data.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.mitkovic.kotlin.remotewave.data.model.Device
import javax.inject.Inject

class DataStoreRepositoryImpl @Inject constructor(
    private val dataStore: DataStore<Preferences>,
    private val gson: Gson,
) : DataStoreRepository {

    companion object {
        private val COOKIES_KEY = stringPreferencesKey("cookies")
        private val DEVICE_KEY = stringPreferencesKey("device")
    }

    override fun getCookies(): Flow<String?> =
        dataStore.data.map { preferences ->
            preferences[COOKIES_KEY]
        }

    override suspend fun saveCookies(cookieString: String) {
        dataStore.edit { preferences ->
            preferences[COOKIES_KEY] = cookieString
        }
    }

    override fun getDevice(): Flow<Device?> =
        dataStore.data
            .map { preferences ->
                preferences[DEVICE_KEY]?.let {
                    gson.fromJson(it, Device::class.java)
                }
            }

    override suspend fun saveDevice(device: Device) {
        val deviceString = gson.toJson(device)
        dataStore.edit { preferences ->
            preferences[DEVICE_KEY] = deviceString
        }
    }
}
