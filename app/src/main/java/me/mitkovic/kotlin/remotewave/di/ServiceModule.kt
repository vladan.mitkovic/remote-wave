package me.mitkovic.kotlin.remotewave.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import me.mitkovic.kotlin.remotewave.common.ConnectivityObserver
import me.mitkovic.kotlin.remotewave.common.NetworkConnectivityObserver
import me.mitkovic.kotlin.remotewave.data.network.DeviceDiscoveryClient
import me.mitkovic.kotlin.remotewave.data.network.SimpleHttpClient
import me.mitkovic.kotlin.remotewave.data.repository.DataStoreRepository
import me.mitkovic.kotlin.remotewave.data.repository.DeviceDiscoveryRepository
import me.mitkovic.kotlin.remotewave.data.repository.DeviceDiscoveryRepositoryImpl
import me.mitkovic.kotlin.remotewave.data.repository.DeviceRepository
import me.mitkovic.kotlin.remotewave.data.repository.DeviceRepositoryImpl
import me.mitkovic.kotlin.remotewave.data.utils.DeviceDescriptionParser
import me.mitkovic.kotlin.remotewave.logging.AppLogger
import me.mitkovic.kotlin.remotewave.tvcontrol.TVControlService
import me.mitkovic.kotlin.remotewave.tvcontrol.TVControlServiceImpl
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()

    @Provides
    @Singleton
    fun provideTVControlService(
        okHttpClient: OkHttpClient,
        dataStoreRepository: DataStoreRepository,
        logger: AppLogger,
    ): TVControlService = TVControlServiceImpl(okHttpClient, dataStoreRepository, logger)

    @Provides
    @Singleton
    fun provideDeviceDiscoveryClient(
        deviceRepository: DeviceRepository,
        logger: AppLogger,
    ): DeviceDiscoveryClient = DeviceDiscoveryClient(deviceRepository, logger)

    @Provides
    @Singleton
    fun provideDeviceDiscoveryRepository(deviceDiscoveryClient: DeviceDiscoveryClient): DeviceDiscoveryRepository =
        DeviceDiscoveryRepositoryImpl(deviceDiscoveryClient)

    @Provides
    @Singleton
    fun provideSimpleHttpClient(logger: AppLogger): SimpleHttpClient = SimpleHttpClient(logger)

    @Provides
    @Singleton
    fun provideDeviceDescriptionParser(): DeviceDescriptionParser = DeviceDescriptionParser()

    @Provides
    @Singleton
    fun provideDeviceRepository(
        simpleHttpClient: SimpleHttpClient,
        parser: DeviceDescriptionParser,
        logger: AppLogger,
    ): DeviceRepository = DeviceRepositoryImpl(simpleHttpClient, parser, logger)

    @Provides
    @Singleton
    fun provideConnectivityObserver(
        @ApplicationContext context: Context,
    ): ConnectivityObserver = NetworkConnectivityObserver(context)
}
