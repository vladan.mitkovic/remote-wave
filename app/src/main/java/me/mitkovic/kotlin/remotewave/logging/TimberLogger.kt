package me.mitkovic.kotlin.remotewave.logging

import timber.log.Timber
import javax.inject.Inject

class TimberLogger @Inject constructor() : AppLogger {

    override fun logDebug(
        tag: String,
        message: String,
    ) {
        Timber.tag(tag).d(message)
    }

    override fun logInfo(
        tag: String,
        message: String,
    ) {
        Timber.tag(tag).i(message)
    }

    override fun logError(
        tag: String,
        message: String?,
        throwable: Throwable?,
    ) {
        val errorMessage = throwable?.localizedMessage ?: message ?: "Unknown error"
        Timber.tag(tag).e(errorMessage)
    }
}
