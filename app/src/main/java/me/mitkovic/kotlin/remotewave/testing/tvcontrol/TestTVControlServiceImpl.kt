package me.mitkovic.kotlin.remotewave.testing.tvcontrol

import me.mitkovic.kotlin.remotewave.data.model.Device
import me.mitkovic.kotlin.remotewave.tvcontrol.TVControlService

object TestTVControlServiceImpl : TVControlService {

    private var authenticationResult: Result<Unit> = Result.success(Unit)
    private var sendCommandResult: Result<Unit> = Result.success(Unit)
    private var storedDevice: Device? = null

    fun setAuthenticationResult(result: Result<Unit>) {
        authenticationResult = result
    }

    fun setSendCommandResult(result: Result<Unit>) {
        sendCommandResult = result
    }

    override suspend fun authenticate(device: Device): Result<Unit> = authenticationResult

    fun getStoredDevice(): Device? = storedDevice

    override fun setTvIpAddress(device: Device) {
        storedDevice = device
    }

    override suspend fun sendCommandToTv(command: String) {
        if (sendCommandResult.isFailure) {
            throw sendCommandResult.exceptionOrNull() ?: Exception("Unknown error")
        }
        println("Command sent to TV: $command")
    }
}
