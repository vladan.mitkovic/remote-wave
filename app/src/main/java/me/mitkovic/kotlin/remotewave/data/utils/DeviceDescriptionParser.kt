package me.mitkovic.kotlin.remotewave.data.utils

import android.util.Xml
import me.mitkovic.kotlin.remotewave.data.model.DeviceDescription
import me.mitkovic.kotlin.remotewave.data.model.IRCCCode
import me.mitkovic.kotlin.remotewave.data.model.Icon
import me.mitkovic.kotlin.remotewave.data.model.ScalarWebAPIService
import me.mitkovic.kotlin.remotewave.data.model.Service
import org.xmlpull.v1.XmlPullParser
import java.io.StringReader

class DeviceDescriptionParser {

    fun parse(xmlContent: String): DeviceDescription {
        val parser = Xml.newPullParser()
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
        parser.setInput(StringReader(xmlContent))

        var eventType = parser.eventType
        var currentTag: String? = null

        var friendlyName: String? = null
        var manufacturer: String? = null
        var manufacturerURL: String? = null
        var modelName: String? = null
        var udn: String? = null

        val icons = mutableListOf<Icon>()
        val services = mutableListOf<Service>()
        val scalarWebAPIServices = mutableListOf<ScalarWebAPIService>()
        val irccCodes = mutableListOf<IRCCCode>()

        var iconMimetype: String? = null
        var iconWidth: Int? = null
        var iconHeight: Int? = null
        var iconDepth: Int? = null
        var iconUrl: String? = null

        var serviceType: String? = null
        var serviceId: String? = null
        var scpdUrl: String? = null
        var controlUrl: String? = null
        var eventSubUrl: String? = null

        var irccCommand: String? = null
        var irccCode: String? = null

        while (eventType != XmlPullParser.END_DOCUMENT) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    currentTag = parser.name
                    when (currentTag) {
                        "icon" -> {
                            iconMimetype = null
                            iconWidth = null
                            iconHeight = null
                            iconDepth = null
                            iconUrl = null
                        }
                        "service" -> {
                            serviceType = null
                            serviceId = null
                            scpdUrl = null
                            controlUrl = null
                            eventSubUrl = null
                        }
                        "av:X_IRCCCode" -> {
                            irccCommand = parser.getAttributeValue(null, "command")
                            irccCode = null
                        }
                    }
                }
                XmlPullParser.TEXT -> {
                    when (currentTag) {
                        "friendlyName" -> friendlyName = parser.text
                        "manufacturer" -> manufacturer = parser.text
                        "manufacturerURL" -> manufacturerURL = parser.text
                        "modelName" -> modelName = parser.text
                        "UDN" -> udn = parser.text
                        "mimetype" -> iconMimetype = parser.text
                        "width" -> iconWidth = parser.text.toInt()
                        "height" -> iconHeight = parser.text.toInt()
                        "depth" -> iconDepth = parser.text.toInt()
                        "url" -> iconUrl = parser.text
                        "serviceType" -> serviceType = parser.text
                        "serviceId" -> serviceId = parser.text
                        "SCPDURL" -> scpdUrl = parser.text
                        "controlURL" -> controlUrl = parser.text
                        "eventSubURL" -> eventSubUrl = parser.text
                        "av:X_ScalarWebAPI_ServiceType" ->
                            scalarWebAPIServices.add(
                                ScalarWebAPIService(parser.text),
                            )
                        "av:X_IRCCCode" -> irccCode = parser.text
                    }
                }
                XmlPullParser.END_TAG -> {
                    when (parser.name) {
                        "icon" -> {
                            if (iconMimetype != null &&
                                iconWidth != null &&
                                iconHeight != null &&
                                iconDepth != null &&
                                iconUrl != null
                            ) {
                                icons.add(
                                    Icon(
                                        iconMimetype,
                                        iconWidth,
                                        iconHeight,
                                        iconDepth,
                                        iconUrl,
                                    ),
                                )
                            }
                        }
                        "service" -> {
                            if (serviceType != null && serviceId != null && scpdUrl != null && controlUrl != null) {
                                services.add(
                                    Service(
                                        serviceType,
                                        serviceId,
                                        scpdUrl,
                                        controlUrl,
                                        eventSubUrl,
                                    ),
                                )
                            }
                        }
                        "av:X_IRCCCode" -> {
                            if (irccCommand != null && irccCode != null) {
                                irccCodes.add(
                                    IRCCCode(
                                        irccCommand,
                                        irccCode,
                                    ),
                                )
                            }
                        }
                    }
                    currentTag = null
                }
            }
            eventType = parser.next()
        }

        return DeviceDescription(
            friendlyName ?: throw IllegalArgumentException("Missing friendlyName"),
            manufacturer ?: throw IllegalArgumentException("Missing manufacturer"),
            manufacturerURL ?: throw IllegalArgumentException("Missing manufacturerURL"),
            modelName ?: throw IllegalArgumentException("Missing modelName"),
            udn ?: throw IllegalArgumentException("Missing udn"),
            icons,
            services,
            scalarWebAPIServices,
            irccCodes,
        )
    }
}
