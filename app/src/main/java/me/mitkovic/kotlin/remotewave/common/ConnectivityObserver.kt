package me.mitkovic.kotlin.remotewave.common

import kotlinx.coroutines.flow.Flow

interface ConnectivityObserver {

    fun observe(): Flow<Status>

    enum class Status {
        Available,
        Lost,
        UnAvailable,
    }
}
