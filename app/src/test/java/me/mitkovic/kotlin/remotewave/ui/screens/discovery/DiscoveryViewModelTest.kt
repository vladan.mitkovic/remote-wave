package me.mitkovic.kotlin.remotewave.ui.screens.discovery

import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDataStoreRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDeviceDiscoveryRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.ui.discovery.TestDiscoveryViewModelProvider
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class DiscoveryViewModelTest {

    private lateinit var viewModel: DiscoveryViewModel
    private val testDispatcher = StandardTestDispatcher()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        viewModel = TestDiscoveryViewModelProvider.provideFakeDiscoveryViewModel()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `start discovery updates UI state correctly`() =
        runTest {
            // Preload the fake devices into the repository
            TestDeviceDiscoveryRepositoryImpl.setDevice(Constants.testDevice)

            viewModel.uiDevicesState.test {
                viewModel.startDiscovery()

                // Expected: Initial state is not loading
                val initialState = awaitItem()
                Assert.assertFalse(initialState.isLoading)

                // Expected: Next state should be loading
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState.isLoading)

                // Expected: Final state, not loading with updated data
                val refreshedState = awaitItem()
                Assert.assertFalse(refreshedState.isLoading)
                Assert.assertTrue(refreshedState.errorMessage.isNullOrEmpty())
                Assert.assertTrue(refreshedState.devices.devices.isNotEmpty())

                // Ensure no more items are emitted
                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `select device saves device to repository`() =
        runTest {
            val testDevice = Constants.testDevice

            TestDataStoreRepositoryImpl.saveDevice(testDevice)

            viewModel.selectDevice(testDevice)

            val savedDevice = TestDataStoreRepositoryImpl.getDevice().first()
            Assert.assertEquals(testDevice, savedDevice)
        }

    @Test
    fun `clear error removes error message`() =
        runTest {
            // Simulate an error state
            viewModel.startDiscovery()
            // Simulate an error occurrence
            viewModel._uiDevicesState.value = viewModel.uiDevicesState.value.copy(errorMessage = "Test Error")

            viewModel.clearError()

            val state = viewModel.uiDevicesState.value
            Assert.assertNull(state.errorMessage)
        }
}
