package me.mitkovic.kotlin.remotewave.ui.screens.control

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.remotewave.common.Constants
import me.mitkovic.kotlin.remotewave.testing.data.repository.TestDataStoreRepositoryImpl
import me.mitkovic.kotlin.remotewave.testing.logging.TestAppLoggerImpl
import me.mitkovic.kotlin.remotewave.testing.tvcontrol.TestTVControlServiceImpl
import me.mitkovic.kotlin.remotewave.testing.ui.control.TestControlViewModelProvider
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ControlViewModelTest {

    private lateinit var viewModel: ControlViewModel
    private lateinit var fakeLogger: TestAppLoggerImpl
    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)

        TestDataStoreRepositoryImpl.setDevice(Constants.testDevice)

        fakeLogger = TestAppLoggerImpl

        // Create the view model once using the provider and the test dispatcher.
        viewModel = TestControlViewModelProvider.provideFakeControlViewModel(ioDispatcher = testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `initialization collects device flow`() =
        runTest {
            val collectedDevice = viewModel.device.first()
            Assert.assertEquals(Constants.testDevice, collectedDevice)
        }

    @Test
    fun `connectToTV sets device on success`() =
        runTest {
            // Configure FakeTVControlServiceImpl for a successful authentication.
            TestTVControlServiceImpl.setAuthenticationResult(Result.success(Unit))

            viewModel.connectToTV(Constants.testDevice)

            Assert.assertNull(viewModel.errorMessage.value)
            Assert.assertEquals(Constants.testDevice, TestTVControlServiceImpl.getStoredDevice())
        }

    @Test
    fun `connectToTV sets error message on failure`() =
        runTest {
            // Configure FakeTVControlServiceImpl to simulate a failure.
            TestTVControlServiceImpl.setAuthenticationResult(Result.failure(Exception("Authentication failed")))

            viewModel.connectToTV(Constants.testDevice)
            Assert.assertEquals("Authentication failed", viewModel.errorMessage.value)
        }

    @Test
    fun `sendCommandToTv sets error message on exception`() =
        runTest {
            // Configure FakeTVControlServiceImpl to simulate an exception.
            TestTVControlServiceImpl.setSendCommandResult(Result.failure(Exception("Command failed")))

            viewModel.sendCommandToTv("POWER")
            Assert.assertEquals("Command failed", viewModel.errorMessage.value)
        }

    @Test
    fun `sendCommandToTv logs error on exception`() =
        runTest {
            TestTVControlServiceImpl.setSendCommandResult(Result.failure(Exception("Command failed")))

            viewModel.sendCommandToTv("POWER")
            Assert.assertTrue(fakeLogger.getLoggedErrors().any { it.contains("Error sending command to TV") })
        }

    @Test
    fun `sendCommandToTv doesn't set error message on success`() =
        runTest {
            TestTVControlServiceImpl.setSendCommandResult(Result.success(Unit))

            viewModel.sendCommandToTv("VOLUME_UP")
            Assert.assertNull(viewModel.errorMessage.value)
        }

    @Test
    fun `clearError resets error message`() =
        runTest {
            viewModel._errorMessage.value = "Some error"
            viewModel.clearError()
            Assert.assertNull(viewModel.errorMessage.value)
        }
}
